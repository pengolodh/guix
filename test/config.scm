;; this file is used to import all the submodules of this machine and is used as the main config.scm
(define-module (test config) 
  #:use-module (gnu)
  #:use-module (gnu system)
  #:use-module (test system core)
  #:use-module (test system filesystems)
  #:use-module (test system networking)
  #:use-module (test system packages)
  #:use-module (test system services)
  #:use-module (test system users)
)
(define test_config
  (operating-system
    (inherit test_core test_filesystems test_networking test_packages test_services test_users)
  )
)
                





  
