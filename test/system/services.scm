(define-module (test system services)
    #:use-module (gnu)
    #:use-module (gnu services docker)
    #:use-module (gnu services networking)
    #:use-module (gnu services virtualization)
    #:export (test_services)    
)
(use-service-modules networking ssh)
(define test_services
    (operating-system       
        ;; Below is the list of system services.  To search for available
        ;; services, run 'guix system search KEYWORD' in a terminal.
        ;; see https://guix.gnu.org/manual/en/guix.html#Services for info about service configuration
        (services
            (append (list
                ;; To configure OpenSSH, pass an 'openssh-configuration'
                ;; record as a second argument to 'service' below.
                (service openssh-service-type)
                (service network-manager-service-type)
                (service wpa-supplicant-service-type)
                (service ntp-service-type)
                (service libvirt-service-type
                    (libvirt-configuration
                        (unix-sock-group "libvirt")
                        (tls-port "16555")
                    )
                )
                ;; This is the default list of services we
                ;; are appending to.
            ) %base-services)
        )
    )
)