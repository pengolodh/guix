(define-module (test system users)
    #:use-module (gnu)
    #:export (test_users)    
)
(define test_users
    (operating-system
    ;; The list of user accounts ('root' is implicit).
        (users (cons* 
                (user-account
                    (name "pengolodh")
                    (comment "Pengolodh")
                    (group "users")
                    (home-directory "/home/pengolodh")
                    (supplementary-groups '("wheel" "netdev" "audio" "video" "kvm" "libvirt"))
                )
        %base-user-accounts))
    )
)