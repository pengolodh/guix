(define-module (test system networking)
    #:use-module (gnu)
    #:use-module (gnu packages vim)
    #:use-module (gnu packages version-control)
    #:use-module (gnu packages package-management)
    #:export (test_packages)
)
(use-package-modules shells)
(use-package-modules certs)

(define test_packages
    (operating-system   
    ;; Packages installed system-wide.  Users can also install packages
    ;; under their own account: use 'guix search KEYWORD' to search
    ;; for packages and 'guix install PACKAGE' to install a package.
        (packages (append (map specification->package ( 
            ;; he specification->package procedure of the (gnu packages) module, which returns the best package for a given name or name and version (https://guix.gnu.org/manual/en/html_node/Using-the-Configuration-System.html)
            ;;https certificates
            "nss-certs"
            ;; basic file-system utils
            "fuse-exfat"
            "exfat-utils"
            ;; basic terminal utils
            "htop"
            "fish"
            "bat"
            "exa"
            "stow"
            "vim"
            "git"
        )))%base-packages)
    )
)