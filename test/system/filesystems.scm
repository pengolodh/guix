(define-module (test system filesystems)
    #:use-module (gnu)
    #:export (test_filesystems)
)
(define test_filesystems
    (operating-system    
        ;; The list of file systems that get "mounted".  The unique
        ;; file system identifiers there ("UUIDs") can be obtained
        ;; by running 'blkid' in a terminal.
        (file-systems (cons* 
            (file-system
                (mount-point "/home")
                (device (uuid"478d9bd2-efe5-4a83-8c20-e417c892ab6a" 'btrfs))
                (type "btrfs")
            )
            (file-system
                (mount-point "/")
                (device (uuid "3ac8041f-bac5-4822-83e1-089439d5e58d" 'xfs))
                (type "xfs")
            ) 
        %base-file-systems))

        ;; define swap space
        (swap-devices (list 
            (swap-space 
                (target (uuid "352d812b-acdb-44c6-b5c4-3dfd65533222"))
            )
        ))
    )
)