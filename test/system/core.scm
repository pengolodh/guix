(define-module (test system core)
    #:use-module (gnu)
    #:use-module (gnu system nss)
    #:use-module (gnu packages linux)
    #:use-module (nongnu packages linux)
    #:use-module (nongnu system linux-initrd)
    #:export (test_core)
)
(define test_core
    (operating-system
        ;; this file contains the bootloader and kernel configuration of the guix system, as well as the locale and timezone
        (bootloader (bootloader-configuration
            (bootloader grub-efi-bootloader)
            (targets '("/boot/efi"))
            (keyboard-layout keyboard-layout)
        ))
        ;; nonfree linux kernel
        (kernel linux)
        (firmware (list linux-firmware))
        (initrd microcode-initrd)
        (initrd-modules (append '("mptspi") %base-initrd-modules))

        (locale "en_US.utf8")
        (timezone "Europe/Brussels")
        (keyboard-layout (keyboard-layout "us"))
    )
)