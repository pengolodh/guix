(define-module (test system networking)
    #:use-module (gnu)
    #:use-module (gnu services networking)
    #:export (test_networking)
)
(use-service-modules networking)
(define test_networking
    (operating-system 
        (host-name "vm-guix-test")
    )
)